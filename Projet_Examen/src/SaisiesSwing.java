import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;


import javax.swing.*;

public class SaisiesSwing extends JFrame {

	private static final long serialVersionUID = 1L;
	
	final private JLabel abscisse = new JLabel("Abscisse : ");
	final private JLabel ordonnee = new JLabel("Ordonnee : ");
	final private JLabel valeur = new JLabel("Valeur : ");
	final private JTextField absTxt = new JTextField(25);
	final private JTextField ordTxt = new JTextField(25);
	final private JTextField valTxt = new JTextField(25);
	
	final private JButton bValider = new JButton("Valider");
	final private JButton bEffacer = new JButton("Effacer");
	final private JButton bTerminer = new JButton("Terminer");
	final private File f = new File("output.txt");

	
	public SaisiesSwing() {
		super();
		
		// Creation de la fenetre
		this.setTitle("Examen NFP121");
	    this.setLocationRelativeTo(null);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);             
	    
		// Insertion des composants de la fen�tre
		JPanel conteneur = new JPanel();
	    JPanel component1 = new JPanel();
		JPanel component2 = new JPanel();
		this.setContentPane(conteneur);
		
		abscisse.setHorizontalAlignment(JLabel.RIGHT);
		ordonnee.setHorizontalAlignment(JLabel.RIGHT);
		valeur.setHorizontalAlignment(JLabel.RIGHT);
		
		component1.setLayout(new GridLayout(0,2));
		component1.add(abscisse);
		component1.add(absTxt);
		component1.add(ordonnee);
		component1.add(ordTxt);
		component1.add(valeur);
		component1.add(valTxt);
		
		
		component2.add(bValider);
		component2.add(bEffacer);
		component2.add(bTerminer);
		
		conteneur.setLayout(new GridLayout(0,1));
		conteneur.add(component1);
		conteneur.add(component2);
		
		this.pack();
		this.setVisible(true);
		
		bTerminer.setEnabled(false);
		this.bTerminer.addActionListener(new ActionTerminer());
		this.bEffacer.addActionListener(new ActionEffacer());
		this.bValider.addActionListener(new ActionValider());
		
		
    	
	}
	
	public class ActionTerminer implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			System.out.println("Fermeture ...");
			System.exit(1);
		}
		
	}
	
	public class ActionEffacer implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == bEffacer){
				System.out.println("Nettoyage des valeurs ! ");
				absTxt.setText("");
				ordTxt.setText("");
				valTxt.setText("");
			}
		}
	}
	
	public class ActionValider implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			boolean isValidField = true;
			if (! isIntValid(absTxt.getText())) {
				absTxt.setBackground(Color.RED);
				isValidField = false;
			}
			else {
				absTxt.setBackground(Color.WHITE);
			}
			
			if (! isIntValid(ordTxt.getText())) {
				ordTxt.setBackground(Color.RED);
				isValidField = false;
			}
			else {
				ordTxt.setBackground(Color.WHITE);
			}
			
			if (! isDoubleValid(valTxt.getText())) {
				valTxt.setBackground(Color.RED);
				isValidField = false;
			}
			else {
				valTxt.setBackground(Color.WHITE);
			}
			
			if (isValidField) {
				System.out.println(absTxt.getText());
				System.out.println(ordTxt.getText());
				System.out.println(valTxt.getText());
				bTerminer.setEnabled(true);
				save(f, absTxt.getText(),ordTxt.getText(),valTxt.getText());
			}
			else {
				System.out.println("Tous les champs ne sont pas valide ! ");
				bTerminer.setEnabled(false);
			}
		}
	}


	public void save(File f, String abs, String ord, String val){
		FileWriter out;
		try {
			out = new FileWriter(f, true);
			PrintWriter pw = new PrintWriter(out);
			pw.print(abs + " ");
			pw.print(ord + " ");
			pw.println(val + " ");
			pw.close();
		} catch (IOException e) {
			System.out.println("Problème d'écriture fichier.");
		}
    	
	}
	
	
	private boolean isIntValid(String valeur)
	{
		try {
			Integer.parseInt(valeur);
		} catch (NumberFormatException f) {
			return false;
		}
		
		return true;
	}
	
	public boolean isDoubleValid(String valeur) {
		try {
			Double.parseDouble(valeur);
		} catch (NumberFormatException f) {
			return false;
		}
		
		return true;

	}
	


	public static void main(String[] args) {
		new SaisiesSwing();
	}
}
