
public class Diviseur extends Traitement {
	private double facteur;
		
	// Construit 
		public Diviseur(double facteur) {
			try {
				this.facteur = facteur;
			} catch (ArithmeticException ae) {
		        System.out.println("Division par zéro !!");
		        System.exit(1);
		    }
			
		}
	
		public void traiter(Position position, double valeur) {
			valeur = valeur/this.facteur;
			super.traiter(position, valeur);
		}

}
