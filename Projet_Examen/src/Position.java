/** Définir une position.  */
public class Position {
	private int x;
	private int y;

	public Position(int x, int y) {
		this.x = x;
		this.y = y;
		// System.out.println("...appel à Position(" + x + "," + y + ")" + " --> " + this);
	}

	@Override public String toString() {
		return super.toString() + "(" + x + "," + y + ")";
	}
	
	public int getX() {
		return this.x;
	}
	
	public int getY() {
		return this.y;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Position ) {
			Position p = (Position) obj;
			return (this.x == p.x && this.y == p.y);
		}
		else {
			return false;
		}
		
	}
}
