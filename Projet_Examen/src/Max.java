/**
  * Max calcule le max des valeurs vues, quelque soit le lot.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */

public class Max extends Traitement {

	private double valeurMax;
	
	
	public void traiter(Position position, double valeur) {
		if(valeur > this.valeurMax) {
			valeurMax = valeur;
		}
		super.traiter(position, valeur);
	}
	
	@Override
	public void gererFinLotLocal(String nomLot) {
		// Affichage format : lot1: max = 18.0
		System.out.println(nomLot + ": max = " + this.valeurMax);
	}
	
	public double max() {
		return this.valeurMax;
	}

	public double getMax() {
		return this.valeurMax;
	}

}
