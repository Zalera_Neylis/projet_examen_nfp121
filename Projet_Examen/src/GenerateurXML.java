import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;


public class GenerateurXML extends Traitement{
	private static String nomFichier;
	private ArrayList<Donnee> donnees= new ArrayList<Donnee>();


	public GenerateurXML(String nomFichier) {
		super();
		GenerateurXML.nomFichier = nomFichier;
	}

	public static void afficherConsole(Document document, OutputStream out) {
		try {
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			sortie.output(document, out);
		} catch (java.io.IOException e) {
			throw new RuntimeException("Erreur sur écriture : ", e);
		}
	}

	public static void genererFichier(Document document, OutputStream out) {
		try {
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			sortie.output(document, new FileWriter(nomFichier));
		}
		catch (IOException io) 
		{
			io.printStackTrace();
		} 
	}

	public void traiter(Position position, double valeur) {
		Element racine = new Element("donnees");
		Donnee nouvelleDonnee = new Donnee(position, valeur);
		this.donnees.add(nouvelleDonnee);
		for (int i=0; i<this.donnees.size(); i++)
		{
			Element donnee = new Element("donnee");
			Attribute id = new Attribute("id", Integer.toString(i));
			donnee.setAttribute(id);

			Attribute y = new Attribute("y", Integer.toString(this.donnees.get(i).getPosition().getY()));
			donnee.setAttribute(y);

			Element x = new Element("x").setText(Integer.toString(this.donnees.get(i).getPosition().getX()));
			Element value = new Element("valeur").setText(Double.toString(this.donnees.get(i).getValeur()));
			donnee.addContent(x);
			donnee.addContent(value);
			racine.addContent(donnee);
		}
		System.out.println("xml");
		Document document = new Document(racine);
		afficherConsole(document, System.out);
		genererFichier(document, System.out);
		super.traiter(position, valeur);
	}
}
