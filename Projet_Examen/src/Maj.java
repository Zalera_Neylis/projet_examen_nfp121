import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Maj indique pour chaque lot les positions mises à jour (ou ajoutées)
 * lors du traitement de ce lot.
 *
 * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class Maj extends Traitement {

	private Map<String, ArrayList<Position>> position;

	public Maj() {
		this.setPosition(new HashMap<String, ArrayList<Position>>());
	}

	@Override
	public void traiter(Position position, double valeur) {
		Objects.requireNonNull(position, "La position ne doit pas etre nul ! .");
		super.traiter(position, valeur);
	}

	public Map<String, ArrayList<Position>> getPosition() {
		return position;
	}

	public void setPosition(Map<String, ArrayList<Position>> position) {
		this.position = position;
	}
	
}

