
public class Donnee extends Donnees{
	
	private Position position;
	private double valeur;
	

	public Donnee(Position position, double valeur) {
		super();
		this.position = position;
		this.setValeur(valeur);
	}

	public Position getPosition() {
		return this.position;
	}
	
	public void setPosition(Position position) {
		this.position = position;
	}

	public double getValeur() {
		return valeur;
	}

	public void setValeur(double valeur) {
		this.valeur = valeur;
	}


}
