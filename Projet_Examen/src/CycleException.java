public class CycleException extends RuntimeException {


	private static final long serialVersionUID = 1L;
	// Constructeur par défaut
	public CycleException()  {
		System.out.println("Le traitement ajouté génère un cycle ! ");
		System.out.println("Fin du traitement ! ");
		System.exit(1);
	}
	// Constructeur avec message personnalisé
	public CycleException(String evt) {
		super(evt);
	}
}
