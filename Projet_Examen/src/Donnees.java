import java.util.ArrayList;

/**
  * Donnees enregistre toutes les données reçues, quelque soit le lot.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */
public class Donnees extends Traitement {

    private ArrayList<Donnee> donnees= new ArrayList<Donnee>();

    @Override
    public void traiter(Position position, double valeur) {
        Donnee nouvelleDonnee = new Donnee(position, valeur);
        this.donnees.add(nouvelleDonnee);
        super.traiter(position, valeur);
    }

    // getter 
    public ArrayList<Donnee> getDonnees() {
        return donnees;
    }
    // setter
    public void setDonnees(ArrayList<Donnee> donnees) {
        this.donnees = donnees;
    }
}