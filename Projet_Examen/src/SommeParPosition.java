import java.util.*;

public class SommeParPosition extends Traitement {

	private HashMap<Position, Double> dictPosVal = new HashMap<Position, Double>();

	
	@Override
	public void traiter(Position position, double valeur) {
		if (dictPosVal.containsKey(position)) {
			
			double somme = dictPosVal.get(position) + valeur;
			dictPosVal.put(position, somme);
		} 
		else {
			
			dictPosVal.put(position, valeur);
		}
		super.traiter(position, valeur);
	}

	public double sommeParPosition(Position position) {
		return this.dictPosVal.get(position);
	}

}
