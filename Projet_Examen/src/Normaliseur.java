
/**
  * Normaliseur normalise les donnÃ©es d'un lot en utilisant une transformation affine.
  *
  * @author	Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
  */
public class Normaliseur extends Traitement {
	private double debut;
	private double fin;
	private Max max;
	private Min min;

	public Normaliseur(double debut, double fin) {
		this.debut = debut;
		this.fin = fin;
	}
	
	@Override
	protected void gererDebutLotLocal(String nomLot) {
		this.max = new Max();
		this.min = new Min();
	}
	
	@Override
	public void traiter(Position position, double valeur) {
		this.max.traiter(position, valeur);
		this.min.traiter(position, valeur);
		
		double max = this.max.getMax();
		double min = this.min.getMin();
		double a = (max - min) / (this.fin - this.debut);
		double b = this.debut - (a * min);
		
		valeur = (a * valeur) + b;
		
		super.traiter(position, valeur);
	}
}
