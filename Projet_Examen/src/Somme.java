/**
  * Somme calcule la sommee des valeurs, quelque soit le lot.
  *
  * @author	Xavier Cr�gut <Prenom.Nom@enseeiht.fr>
  */

public class Somme extends SommeAbstrait {

	private double somme;


	@Override
	public void gererFinLotLocal(String nomLot) {
		System.out.println(nomLot + ": somme = " + this.somme());
	}
	
	@Override
	public double somme() {
		return this.somme;
	}
	
	public void traiter(Position position, double valeur) {
		this.somme += valeur;
		super.traiter(position, valeur);
	}

}
