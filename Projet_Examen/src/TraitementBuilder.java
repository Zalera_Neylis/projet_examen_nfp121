import java.lang.reflect.*;
import java.util.*;

/**
  * TraitementBuilder 
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */
public class TraitementBuilder {
	
	/** Retourne un objet de type Class correspondant au nom en paramètre.
	 * Exemples :
	 *   - int donne int.class
	 *   - Normaliseur donne Normaliseur.class
	 */
	Class<?> analyserType(String nomType) throws ClassNotFoundException {
		
		if (nomType.contains("int")) {
			return int.class;
		} else if (nomType.contains("double")) {
			return double.class;
		} else if (nomType.contains("String")) {
			return String.class;
		} else {
			return Class.forName(nomType);
		}
	}

	/** Crée l'objet java qui correspond au type formel en exploitant le « mot » suviant du scanner.
	 * Exemple : si formel est int.class, le mot suivant doit être un entier et le résulat est l'entier correspondant.
	 * Ici, on peut se limiter aux types utlisés dans le projet : int, double et String.
	 */
	static Object decoderEffectif(Class<?> formel, Scanner in) {
		String inValue = in.next();
		if (formel.equals(int.class)) {
			return Integer.parseInt(inValue);
		} else if (formel.equals(double.class)) {
			return Double.parseDouble(inValue);
		} else {
			return formel.cast(inValue);
		}
		
	}

	/** Définition de la signature, les paramètres formels, mais aussi les paramètres formels.  */
	static class Signature {
		Class<?>[] formels;
		Object[] effectifs;

		public Signature(Class<?>[] formels, Object[] effectifs) {
			this.formels = formels;
			this.effectifs = effectifs;
		}
	}

	/** Analyser une signature pour retrouver les paramètres formels et les paramètres effectifs.
	 * Exemple « 3 double 0.0 String xyz int -5 » donne
	 *   - [double.class, String.class, int.class] pour les paramètres effectifs et
	 *   - [0.0, "xyz", -5] pour les paramètres formels.
	 */
	Signature analyserSignature(Scanner in) throws ClassNotFoundException {
		// Récupartion du premier mot comme nombre de paramètre
		int params = in.nextInt();

		Class<?>[] classArray = new Class<?>[params];
		Object[] objArray = new Object[params];

		for (int i = 0; i < params; i++) {
			classArray[i] = analyserType(in.next());
			objArray[i] = decoderEffectif(classArray[i], in);
		}
		return new Signature(classArray, objArray);

	}


	/** Analyser la création d'un objet.
	 * Exemple : « Normaliseur 2 double 0.0 double 100.0 » consiste à charger
	 * la classe Normaliseur, trouver le constructeur qui prend 2 double, et
	 * l'appeler en lui fournissant 0.0 et 100.0 comme paramètres effectifs.
	 */
	Object analyserCreation(Scanner in) throws ClassNotFoundException, InvocationTargetException,
	IllegalAccessException, NoSuchMethodException,InstantiationException
	{
		// Récupération du nom du traitement
				String typeTraitement =  in.next();
				// Création de la classe à partir du nom
				Class<?> classTraitement = Class.forName(typeTraitement);
				// Création de la signature du traitement
				Signature signature = analyserSignature(in);
				// Création du traitement
				Constructor<?> constructeur = classTraitement.getConstructor(signature.formels);
				Object traitement = constructeur.newInstance(signature.effectifs);

				return traitement;
			}



	/** Analyser un traitement.
	 * Exemples :
	 *   - « Somme 0 0 »
	 *   - « SupprimerPlusGrand 1 double 99.99 0 »
	 *   - « Somme 0 1 Max 0 0 »
	 *   - « Somme 0 2 Max 0 0 SupprimerPlusGrand 1 double 20.0 0 »
	 *   - « Somme 0 2 Max 0 0 SupprimerPlusGrand 1 double 20.0 1 Positions 0 0 »
	 * @param in le scanner à utiliser
	 * @param env l'environnement où enregistrer les nouveaux traitements
	 */
	Traitement analyserTraitement(Scanner in, Map<String, Traitement> env) throws ClassNotFoundException, 
	InvocationTargetException, IllegalAccessException, NoSuchMethodException, InstantiationException{
	
		Traitement treatment = (Traitement) analyserCreation(in);
		// Mise en place d'un boucle récursive en fonction du nombre de traitements
		int nbNextTreatment = in.nextInt();
		for (int i = 0; i < nbNextTreatment; i++) {
			treatment.ajouterSuivants(analyserTraitement(in, env));
		}
		return treatment;
	}


	/** Analyser un traitement.
	 * @param in le scanner à utiliser
	 * @param env l'environnement où enregistrer les nouveaux traitements
	 */
	public Traitement traitement(Scanner in, Map<String, Traitement> env)
	{
		try {
			return analyserTraitement(in, env);
		} catch (Exception e) {
			throw new RuntimeException("Erreur sur l'analyse du traitement, "
					+ "voir la cause ci-dessous", e);
		}
	}
	
	public static void main(String[] args) {
		Traitement resultat = new TraitementBuilder().traitement(new Scanner("Somme 0 2 Max 0 0 Positions 0 0"), null);
		System.out.println(resultat);
	}

}
