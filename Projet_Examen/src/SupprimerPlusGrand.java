/**
  * SupprimerPlusGrand supprime les valeurs plus grandes qu'un seuil.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */
public class SupprimerPlusGrand extends Traitement {

	private double seuil;
		
	public SupprimerPlusGrand(double seuil) {
		this.seuil = seuil;
	}
	
	public void traiter(Position position, double valeur) {
		if (valeur <= seuil) {
			super.traiter(position, valeur);
		}
	}
	
	@Override
	public void gererFinLotLocal(String nomLot) {
		// Affichage format : lot1: max = 18.0
		System.out.println(nomLot + ": seuil = " + this.seuil);
	}
}
