import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.AbstractMap.SimpleImmutableEntry;

public class readDataTxt2 {
	private ArrayList<SimpleImmutableEntry<Position, Double>> source = new ArrayList<SimpleImmutableEntry<Position, Double>>();
    final String fileTxt = "src/donnees-erreurs.txt";
    
	public final ArrayList<SimpleImmutableEntry<Position, Double>> getSource() {
		return source;
	}

	/** Charger l'analyseur avec les donnees du fichier "donnees.txt". */
	public void traiter() {
		try (BufferedReader in = new BufferedReader(new FileReader(this.fileTxt))) {
			String row = null;
			while ((row = in.readLine()) != null) {
				String[] element = row.split("\\s+");
				assert element.length == 5;	
				double value = Double.parseDouble(element[4]);
				int x = Integer.parseInt(element[1]);
				int y = Integer.parseInt(element[2]);
				Position position = new Position(x, y);
				SimpleImmutableEntry<Position, Double> input =  new SimpleImmutableEntry<Position, Double>(position, value);
				this.source.add(input);

			}
			System.out.println(this.source);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static void main(String[] args) {
		readDataTxt2 test = new readDataTxt2();
		test.traiter();
	}
}